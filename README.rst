=================================
 stupid -- an aptly named package
=================================

This is a stupid package.  It's only purpose in life is to serve as a stupid
example of some basic Pythonistry: testing, setup, extension modules, etc.


Author
======

`stupid` is Copyright (C) 2010-2022 Barry Warsaw <barry@python.org>

Licensed under the terms of the Apache License Version 2.0.  See the LICENSE
file for details.


Project details
===============

 * Project home: https://gitlab.com/warsaw/stupid
 * Report bugs at: https://gitlab.com/warsaw/stupid/issues
 * Code hosting: https://gitlab.com/warsaw/stupid.git
 * Documentation: https://stupid.readthedocs.io
 * PyPI: https://pypi.python.org/pypi/stupid
