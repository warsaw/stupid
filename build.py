# Build the _stupid.c extension module
# https://pdm.fming.dev/pyproject/tool-pdm/#build-c-extensions

from setuptools import Extension


ext_modules = [
    Extension('_stupid', ['_stupid/stupid.c']),
    ]


def build(setup_kwargs):
    setup_kwargs.update(ext_modules=ext_modules)
