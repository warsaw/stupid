from public import public


__version__ = '4.0'


import _stupid


@public
class Stupid:
    """Something stupid."""

    @property
    def yes(self) -> str:
        return _stupid.yes()

    @property
    def no(self) -> str:
        return _stupid.no()


@public
def yes() -> None:
    print(Stupid().yes)


@public
def no() -> None:
    print(Stupid().no)
