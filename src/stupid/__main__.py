"""stupid script main entry point."""

import sys
import argparse

from typing import List, Optional

from public import public

import stupid


@public
def main(argv: Optional[List[str]] = None) -> int:
    if argv is None:
        argv = sys.argv[1:]
    parser = argparse.ArgumentParser(
        prog='stupid', description='A stupid script'
    )
    parser.set_defaults(answers=[])
    parser.add_argument(
        '--version', action='version', version=f'stupid {stupid.__version__}'
    )
    parser.add_argument(
        '-y',
        '--yes',
        action='append_const',
        const='yes',
        dest='answers',
        help='Yes',
    )
    parser.add_argument(
        '-n',
        '--no',
        action='append_const',
        const='no',
        dest='answers',
        help='No',
    )
    args = parser.parse_args(argv)
    for answer in args.answers:
        if answer.startswith('y'):
            stupid.yes()
        elif answer.startswith('n'):
            stupid.no()
        else:
            print('?')
    return 0


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
