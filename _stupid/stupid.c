/*
Copyright (C) 2010-2022 Barry A. Warsaw

This file is part of stupid.

stupid is free software: you can redistribute it and/or modify it under the
terms of the GNU Lesser General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

stupid is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License
along with stupid.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Python.h>
#include "stupid.h"

static PyObject *
stupid_yes(PyObject *m)
{
    return PyUnicode_FromString(STUPID_YES);
}


static PyObject *
stupid_no(PyObject *m)
{
    return PyUnicode_FromString(STUPID_NO);
}


static PyMethodDef stupid_methods[] = {
    {STUPID_YES, (PyCFunction)stupid_yes, METH_NOARGS, "Yes"},
    {STUPID_NO,  (PyCFunction)stupid_no,  METH_NOARGS, "No"},
    {NULL, NULL, 0, NULL}       /* Sentinel */
};


static struct PyModuleDef stupid_module = {
    PyModuleDef_HEAD_INIT,
    "_stupid",
    NULL,
    0,                          /* size of module state */
    stupid_methods
};


PyObject *
PyInit__stupid(void)
{
    return PyModule_Create(&stupid_module);
}


/*
 * Local Variables:
 * c-file-style: "python3"
 * End:
 */
