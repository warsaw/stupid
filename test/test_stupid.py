import pytest

import stupid


@pytest.fixture
def stupid_instance():
    return stupid.Stupid()


def test_yes(stupid_instance):
    assert stupid_instance.yes == 'yes'


def test_no(stupid_instance):
    assert stupid_instance.no == 'no'


def test_call_yes(capsys):
    stupid.yes()
    assert capsys.readouterr().out == 'yes\n'


def test_call_no(capsys):
    stupid.no()
    assert capsys.readouterr().out == 'no\n'
